import os
from os import geteuid
from os import system
from time import sleep

import psutil
from colorama import Fore
from pymongo import MongoClient


def u_name():
    uname = ""
    for attr in os.uname():
        uname += (attr + " ")
    print(Fore.LIGHTRED_EX + "UNAME: " + Fore.WHITE + uname)


def cpu_usage():
    print(Fore.LIGHTRED_EX + "CPU: " + Fore.WHITE + str(psutil.cpu_percent()) + "%")
    print(Fore.LIGHTRED_EX + "     " + Fore.WHITE + str(psutil.cpu_freq().max / 1000) + " GHz")


def memory():
    print(
        Fore.LIGHTRED_EX + "Memoria Usada: " + Fore.WHITE + str(round(psutil.virtual_memory().used / (1024 * 1024)))
        + " MB")


def print_properties():
    u_name()
    memory()
    cpu_usage()
    print("\n")


def monitor():
    try:
        MongoClient('localhost', 27017)['admin'].command("serverStatus")
    except:
        return False
    return True


def main():
    print(Fore.CYAN + "Inicializando o Service Monitor...\n")
    sleep(5)

    if geteuid() != 0:
        exit(Fore.RED + "Voce precisa ter previlegios root para executar esse script.\nTente novamente usando 'sudo'.")

    print_properties()

    print(Fore.YELLOW + "Inicializando watchers do MongoDB")
    while True:
        if not monitor():
            print(Fore.RED + "Tentando inicializar a instancia do MongoDB...")
            system("service mongod stop")
            system("service mongod start")
            print(Fore.GREEN + "MongoDB inicializado novamente." + Fore.RESET + "\n")
        sleep(5)


if __name__ == "__main__":
    main()
